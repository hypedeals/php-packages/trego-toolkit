# Toolkit

Toolkit for Trego PHP-based services.

### How To Use
To use Toolkit for the first time, don't forget to constuct the object by type this:

```php
$toolkit = new Toolkit([
    'aws_access_key' => 'your_access_key',
    'aws_secret_key' => 'your_secret_key',
])
```

### Features

 - Amazon Web Services DynamoDB
 - Amazon Web Services Simple Queue Services

### Amazon Web Services DynamoDB

To get configuration key that stored in DynamoDB, you might type this:
```php
$toolkit = $toolkit->getConfig('configuration_key');
```
### Amazon Web Services Simple Queue Services
To send a message into a queue, you might use dispatch() method by type this:
```php
$result = $toolkit->dispatch('queue_name', $payload);
```
Make sure that the queue_name already registered on AWS DynamoDB. For payload attributes, make sure use json_encode before put it into dispatch method.

To receive messages from queue, you might use listen() method by type this:
```php
$result = $toolkit->listen('queue_name', function ($message) {
    // The process goes here.
});
```

