<?php

namespace Trego\Toolkit;

use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;
use Trego\Toolkit\Aws\AwsConnector;
use Aws\Exception\AwsException;

class Toolkit
{
    /**
     * @var AwsConnector
     */
    protected $aws;

    public function __construct(array $config)
    {
        $this->aws = new AwsConnector($config['aws_access_key'], $config['aws_secret_key']);
    }

    /**
     * Get a configuration by key.
     *
     * @param string $key
     * @return string
     */
    public function getConfig($key)
    {
        $dynamodb = $this->aws->create('dynamoDb');
        $marshaler = new Marshaler;
        $json_key = json_encode(['id' => $key]);
        $marshaled_key = $marshaler->marshalJson($json_key);

        try {
            $result = $dynamodb->getItem([
                'TableName' => 'trego-config-dev',
                'Key' => $marshaled_key,
            ]);
            
            return array_values($result['Item']['value'])[0];
        } catch (DynamoDbException $e) {
            return null;
        }
    }

    /**
     * Dispatch message to specific queue.
     *
     * @param string $queue
     * @param string $message
     * @return mixed
     */
    public function dispatch($queue, $message)
    {
        $queue_url = $this->getConfig($queue);

        try {
            $sqs = $this->aws->create('sqs');
            $result = $sqs->sendMessage([
                'MessageBody' => $message,
                'QueueUrl' => $queue_url,
            ]);

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Listen to a queue.
     *
     * @param string $queue
     * @param \Closure $closure
     * @return void
     */
    public function listen($queue, \Closure $closure)
    {
        $queue_url = $this->getConfig($queue);

        try {
            $sqs = $this->aws->create('sqs');
            $result = $sqs->receiveMessage([
                'MaxNumberOfMessages' => 1,
                'QueueUrl' => $queue_url,
            ]);

            $payload = $result->get('Messages');

            if (! is_array($payload) || count($payload) < 1) {
                return;
            }

            $message = array_pop($payload);
            
            if ($closure($message['Body'])) {
                $sqs->deleteMessage([
                    'QueueUrl' => $queue_url,
                    'ReceiptHandle' => $message['ReceiptHandle'],
                ]);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
