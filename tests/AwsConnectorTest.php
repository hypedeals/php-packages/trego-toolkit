<?php

namespace Trego\Toolkit\Test;

use PHPUnit\Framework\TestCase;
use Trego\Toolkit\Aws\AwsConnector;
use Aws\Sqs\SqsClient;

class AwsConnectorTest extends TestCase
{
    protected $config;

    protected $aws;

    public function setUp(): void
    {
        parent::setUp();

        $this->config = require __DIR__ . '/../test-config.php';
        $this->aws = new AwsConnector($this->config['AWS_ACCESS_KEY'], $this->config['AWS_SECRET_KEY']);
    }
}
