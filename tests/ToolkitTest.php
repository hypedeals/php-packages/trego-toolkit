<?php

namespace Trego\Toolkit\Test;

use PHPUnit\Framework\TestCase;
use Trego\Toolkit\Toolkit;
use function GuzzleHttp\json_encode;

class ToolkitTest extends TestCase
{
    protected $toolkit;

    protected $config;

    public function setUp(): void
    {
        parent::setUp();

        $this->config = include __DIR__ . '/../test-config.php';

        $this->toolkit = new Toolkit([
            'aws_access_key' => $this->config['AWS_ACCESS_KEY'],
            'aws_secret_key' => $this->config['AWS_SECRET_KEY'],
        ]);
    }

    public function testGetConfig()
    {
        $config = $this->toolkit->getConfig('USER_SERVICE_URL');

        $this->assertTrue(is_string($config));
    }

    public function testDispatchMessage()
    {
        $payload = [
            'type' => 'USER_VERIFICATION',
            'payload' => [
                'name' => 'Yuri Chandra',
                'email' => 'yurichandra@gmail.com',
                'token' => 'aiwfh9q283h2n',
            ],
        ];

        $result = $this->toolkit->dispatch('QUEUE_USER', json_encode($payload));

        $this->assertEquals($result['MD5OfMessageBody'], md5(json_encode($payload)));
    }

    public function testListenMessage()
    {
        $this->toolkit->listen('QUEUE_USER', function ($message) {
            return true;
        });
    }
}
